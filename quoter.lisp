(defun marshall-hash-table (ht)
  (loop for k being the hash-keys in ht using (hash-value v) collecting
       (cons k
             (if (hash-table-p v)
                 (serialize-hash-table v)
                 v))))

(defun unmarshall-hash-table (alist)
  (let ((new-ht (make-hash-table)))
    (loop for (key . value) in alist do
         (setf (gethash key new-ht)
               (if (listp value)
                   (unmarshall-hash-table value)
                   value)))
    new-ht))

(defun read-file-into-string (file)
  (with-open-file (f file)
    (let ((file-data (make-string (file-length f))))
      (read-sequence file-data f)
      (nsubstitute #\Space #\Newline file-data))))

(defun split-seq (splitp seq)
  (loop
     for beg = (position-if-not splitp seq)
     then (position-if-not splitp seq :start (1+ end))
     for end = (and beg (position-if splitp seq :start beg))
     when beg collect (subseq seq beg end)
     while end))

(defun split-seq-left-preserve (splitp seq)
  (loop
     for beg = (position-if-not splitp seq)
     then (position-if-not splitp seq :start (1+ end))
     for end = (and beg (position-if splitp seq :start beg))
     when beg collect (subseq seq beg (if end (1+ end)))
     while end))

(defun split-sentences (string)
  (split-seq-left-preserve (lambda (x) (position x ".!?")) string))

(defun split-sentence-terminator (sentence)
  (concatenate 'string
	       (subseq sentence 0 (1- (length sentence)))
	       " "
	       (subseq sentence (1- (length sentence)))))

(defun split-words (string)
  (split-seq (lambda (x) (char= #\Space x)) string))

(defun build-word-order-alist (word-list)
  (maplist (lambda (x) (cons (if (first x)
				 (intern (first x) 'keyword))
			     (if (second x)
				 (intern (second x) 'keyword))))
	   (cons nil word-list)))

(defun build-bigram-table (file)
  (declare (optimize (speed 3) (safety 0)))
  (let ((sentences
	 (mapcar #'build-word-order-alist
		 (mapcar
		  (lambda (x) (split-words (split-sentence-terminator x)))
		  (split-sentences (read-file-into-string file)))))
	(bigram-table (make-hash-table)))
    (flet ((update-bigram-table (pair)
	     (declare (optimize (speed 3) (safety 0)))
	     (let ((entry (gethash (car pair) bigram-table)))
	       (unless entry
		 (setf entry (make-hash-table))
		 (setf (gethash (car pair) bigram-table) entry))
	       (let ((word-count (gethash (cdr pair) entry)))
		 (if word-count
		     (incf (the fixnum (gethash (cdr pair) entry)))
		     (setf (gethash (cdr pair) entry) 1))))))
      (dolist (word-pair sentences)
	(mapc #'update-bigram-table word-pair)))
    (with-open-file (out-stream (concatenate 'string (namestring file) ".bigram")
                                :direction :output :if-exists :supersede
                                :if-does-not-exist :create)
      (print (marshall-hash-table bigram-table) out-stream))
    bigram-table))

(defun load-bigram-table (file)
  (let ((cache-name (concatenate 'string (namestring file) ".bigram")))
    (with-open-file (in-stream cache-name)
      (unmarshall-hash-table (read in-stream)))))

(defun use-cache-file-p (file)
  (let ((cache-name (concatenate 'string (namestring file) ".bigram")))
    (and (probe-file cache-name)
         (handler-case (>= (file-write-date cache-name)
                           (file-write-date file))
           (file-error (ex) (declare (ignore ex)) nil)))))

(defun build-bigram-string (file)
  (declare (optimize (speed 3) (safety 0)))
  (let ((bigram-table (if (use-cache-file-p file)
                          (load-bigram-table file)
                          (build-bigram-table file))))
    (flet ((choose-next-word (current-word)
	       (let (tmp next-words)
		 (maphash (lambda (k v)
			    (push (make-list v :initial-element k) tmp))
			  (gethash current-word bigram-table))
		 (setf next-words (reduce #'nconc tmp))
		 (nth (random (length next-words)) next-words))))
      (reduce (lambda (x y)
		(concatenate 'string
			     x
			     (unless (or (null y) (position (char y 0) ".!?"))
			       " ")
			     y))
	      (loop
		 for current-word = (choose-next-word nil)
		 then (choose-next-word current-word)
		 while current-word collect (symbol-name current-word))))))

(defun get-argv ()
  (or #+sbcl (cdr sb-ext:*posix-argv*)))

(setf *random-state* (make-random-state t))

(format t "~a~%" (build-bigram-string (first (get-argv))))
